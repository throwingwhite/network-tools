#!/usr/local/bin/python3
"""Print a list of peers and the config group they are configured under for junos devices
arg1: path to rancid routerdb to obatin list of junos devices """

import sys, re, ipaddress, pathlib
from jnpr.junos import Device
from myTables.ConfigTables import BgpGroupTable,BgpPeerTable,BgpGroupTableRI,BgpPeerTableRI

def main():

	user = "username"
	passwd = "password"

        # check if routerdb exits and parse contents
        if len(argv) == 2:
                #Parse Rancid routerdb file if it exists
                rdbfile = argv[1]

                if pathlib.Path(rdbfile).is_file():
                        rdb = rdb_get(rdbfile)
                else:
                        print("Error: routerdb file does not exist")
        else:
                print("Error: Require path to routerdb")
                exit(1)


	for router in rdb:
		
		print("------"+router+"------")
		dev = Device(router,user=user,password=passwd)
	
		try:
		  dev.open()
		except:
		  print("Connection failed")
		  exit(1)	

                # Obtain a list of BGP config groups
		groups = BgpGroupTable(dev).get()
	
		for group in groups:
                        # For each config group, obtain a list of BGP neighbours 
			peers = BgpPeerTable(dev).get(group=group.group)
			
			print_peers(peers,group)
		try:
                        #Obtain a list of BGP groups configured under routing instances
			groups_ri = BgpGroupTableRI(dev).get()

			for group in groups_ri:
				peers = BgpPeerTableRI(dev).get(group=group.group)

				print_peers(peers,group)
				
		except:
			pass

		dev.close()
	exit()

def rdb_get(repo):
        """ Parse rancid router db file and store all active Junos devices
        routerdb syntax: < hostname;device_type;(up|down) >
        repo: rancid routerdb file path
        return: list of active Junos devices """

        rdb=[]

        rdb_fh = open(repo)
        rdb_list = rdb_fh.read()
        for router in rdb_list.splitlines():
                router = router.strip().split(';')
                if router[0] == '' or router[0][0] == '#' or len(router) != 3:
                        continue
                if router[2] == 'up' and router[1] == 'juniper':
                        if match:
                                rdb.append(router[0])

        return rdb

def print_peers(peers,group):
        """ print a list of peers (short form IP address) and their config group
        peers: list of BGP neighbours
        group: the BGP config group """
        
	for peer in peers:
		peer = ipaddress.ip_address(peer.neighbor).compressed

		if re.search(':0:',peer):
			if not re.search('::', peer):
				peer = re.sub('(:0:.*)(:0:)',r'\1::',peer,1)


		print(peer+", "+ group.group)

main()
