from sys import *
import pexpect, re

class Connect:
	""" Class that uses pexpect to connect to Cisco devices by leveraging existing rancid installation expect
        scripts and login database """

	def __init__(self,host,connect_timeout=10,command_timeout=5):
                """ Create pexpect object for router session using Rancid's clogin expect script
                host: target host
                return: pexpect object """

		self.console = pexpect.spawn('clogin -t ' + str(connect_timeout) + ' ' + host)
		result = self.console.expect(['\n[^\n]*#','Error:.*'])
		if result:
			raise ValueError(self.console.after.decode().rstrip())
		

	def get_mgmt_routes(self,exp,command_timeout=5):
                """ Search config for route pattern and return all matches
                exp: route pattern to match
                return: list of routes """

		routes =[]

		self.console.sendline('sh run | in ip route.*'+exp+"\nshow EOF")
		try:
			self.console.expect('.*\n%')
			match = re.findall(r'\nip route.*',self.console.after.decode())
			if match:
				for route in match:
					route = route.strip().split(' ')
					for i in range(0,len(route)-1):
                                                # Prepend address to the address portion for later matching
						if re.match(exp.split()[0],route[i]):
							route[i]="address"+route[i]
							break
					routes.append(route)
				return routes
			else:
				return None

		except:
			return None
	
	def get_mgmt_acl(self,command_timeout=5):
                """Search for magament ACL and return ACL number
                retuirn: set of ACL numbers  """		
		acls = set()

		self.console.sendline('sh run | in access-class [0-9]\nshow EOF')
		try:
			self.console.expect('.*\n%')
			match = re.findall(r'\n access-class.*',self.console.after.decode())
			if match:
				for acl in match:
					acls.add(acl.split()[1])
				return acls
			else:
				return None

		except:
			return None

	def config(self):
                """ Enter config mode 
                return None if command fails """
		self.console.sendline('configure terminal\n')
		try:
			self.console.expect('.*(config)')
			return self.console.after
		except:
			return None	

	def config_end(self):
                """ Exit config mode
                return None on error """
		self.console.sendline('end\n')
		try:
			self.console.expect('.*#')
			return self.console.after
		except:
			return None
	
	def config_save(self):
                """ Commit running config
                return none on error """
		self.console.sendline('write memory\n')
		try:
			self.console.expect('.*\[OK\]')
			return self.console.after
		except:
			return None

	def delete_mgmt_route(self,route):
		""" Delete management route
                route: a list returned by get_mgmt_route() function """
		mgmt_route = route.copy()		

		for i in range(0,len(mgmt_route)-1):
			if mgmt_route[i].find('address') != -1:
				mgmt_route[i]=mgmt_route[i].lstrip('address')
				break
		command = 'no '+' '.join(mgmt_route)
		self.console.sendline(command)

	def set_mgmt_route(self,route,mask,mroute):
	        """ add route for management prefix
                route: net address
                mask: subnet mask
                mroute: list returned by get_mgmt_route() function """	

		mgmt_route = mroute.copy()

                """ use existing management route config as template
                so that vrf, next-hop, and additional route options are
                preserved """
		for i in range(0,len(mgmt_route)-1):
			if mgmt_route[i].find('address') != -1:
				mgmt_route[i]=route
				mgmt_route[i+1]=mask
				break
		
		command = ' '.join(mgmt_route)
		self.console.sendline(command)
		

	def is_redist(self,type,command_timeout=5):
		""" Check if routes are being redistributed
                type: type or route (static, ospf, etc
                return match object or none on error"""

		self.console.sendline('sh run | in redistribute '+type+'\nshow EOF')

		try:	
			self.console.expect('.*\n%')
			return re.search(r'\n\s*redistribute '+type+r'[^(\r\n)]*',self.console.after.decode())
		except:
			return None

	
	def get_redistribute_acls(self,command_timeout=5):
		""" Check for distribute list statement. Don't need to check for route maps since the only
                existing route maps are configured as white lists and can be safely ignored 
                return: lists of redistribute ACLs"""

		acls=set()		

		self.console.sendline('sh run | in distribute-list.*out|redistribute static.*route\nshow EOF')
		try:
			self.console.expect('.*\n%')
			match = re.findall(r'(\n\s*distribute-list.*|\n\sredistribute static.*route.*)',self.console.after.decode())
			if match:
				for acl in match:
					try:
						acl = acl.strip('\n').split(' ')
						dl_index = acl.index('distribute-list')
				
						if acl[dl_index + 1] == 'prefix':
                                                        # add p and prefix list number
							acls.add('p'+acl[dl_index + 2])
						else:
                                                        # add a and ACL number
							acls.add('a'+acl[dl_index + 1])		
					except:
						acls.add('r00')
				return acls
			else:
				return None
		except:
			return None
	
	def update_redist_acl(self,acl,command_timeout=5):
	        """Obtain ACL sequence number if ACL is of white-list type (permit any)
                acl: the ACL number
                return: squence number """
		sequence = []	
	
		self.console.sendline('sh access-lists '+acl+'\nshow EOF')
		try:
			self.console.expect('.*\n%')
			show_acl = self.console.after.decode()
			match = re.search(r'permit any',show_acl)
			if match:
				acl_list = re.findall(r'(.*(permit|deny).*)',self.console.after.decode())
				for entry in acl_list:
					entry = entry[0].lstrip().split()
					if entry and entry[0].isdigit():
						sequence.append(entry[0])
	
				for seq in range((int(sequence[0])+1),(int(sequence[-1])-1)):
					try:
						sequence.index(str(seq))
						continue
					except:
						return seq
						break
			else:
				return None
		except:
			raise

	def update_mgmt_acl(self,acl,mgmt_net,new_net,command_timeout=5):
		""" Search for ACL for existing prefix and add new prefix
                acl: ACL number
                mgmt_net: existing mgmt prefix
                new_net: prefix to add
                return: match object """

		self.console.sendline('sh access-lists '+acl+'\nshow EOF')
		try:
			self.console.expect('.*\n%')
			show_acl = self.console.after.decode()
			match = re.search(mgmt_net,show_acl)
			if match:
				self.config()
				self.console.sendline('ip access-list standard '+acl)
				self.console.sendline('no deny any')
				self.console.sendline('permit '+new_net)
				self.config_end()
				self.config_save()
				return match
			else:
				return match
		except:
			raise
	
	def close(self):
		self.console.sendline('exit')
