from sys import *
import pexpect, re

class Connect:
	""" Class that uses pexpect to connect to ScreenOS devices by leveraging existing rancid installation expect
        scripts and login database """

	prompt='->'
	send_eof='\nEOF'
	EOF='.*keyword EOF'

	def __init__(self,host,connect_timeout=10,command_timeout=5):
                """ Create pxpect object for session to firewall using Rancid expect script
                host: target host
                return: pexpect object """

		self.console = pexpect.spawn('nlogin -t ' + str(connect_timeout) + ' ' + host)
		result = self.console.expect(['\n[^\n]*'+self.prompt,'Error:.*'])
		if result:
			raise ValueError(self.console.after.decode().rstrip())
		

	def get_mgmt_routes(self,exp,command_timeout=5):
	        """ obtain a template management route by searching for existing management route
                exp: expression to mach for existing management route
                return: list of management routes """
	
		routes =[]

		self.console.sendline('get config | in "set route.*'+exp+'"'+self.send_eof)
		try:
			self.console.expect(self.EOF)
			match = re.findall(r'\nset route.*',self.console.after.decode())
			if match:
				for route in match:
					route = route.strip().split()
					routes.append(route)
				return routes
			else:
				return None

		except:
			return None

	def add_mgmt_prefix(self,mgmt_net,new_net,command_timeout=5):
                """ add manager IP
                mgmt_net: existing management prefix
	        new_net: prefix to add
                return: result string """

		prefixes = []
		
		self.console.sendline('get config | in "manager-ip '+mgmt_net+'"'+self.send_eof)
		try:
			self.console.expect(self.EOF)
			match = re.findall(r'\nset admin manager-ip.*',self.console.after.decode())
                        """ only add manager IP is existing manager IP exists; othewise a new ACL 
                        with only the new prefix and implicit deny will be created """

			if match:
				self.console.sendline('set admin manager-ip '+new_net+self.send_eof)
				self.console.expect(self.EOF)
				pattern = self.console.after.decode()
				if re.findall(r'Maximum', pattern):
					result = "Maximum management hosts reached"
				elif re.findall(r'already configured',pattern):
					result = "MGMT prefix already configured"
				else:
					result = "MGMT prefix successfully added"
				
				return result	
			else:
				return "No MGMT prefix found"

		except:
			return none

	def set_mgmt_route(self,route,mgmt_route):
		pass
	
	def get_vrouters(self):
                """ Return a list of vrouters """	
	
		vrouters=[]		

		self.console.sendline('get vrouter'+self.send_eof)

		try:	
			self.console.expect(self.EOF)
			match = re.findall(r'[1-9]\s\w+.*Root',self.console.after.decode())
			for vrouter in match:
				vrouter = vrouter.split()
				vrouters.append(vrouter[1])
			return vrouters
		except:
			return None

	def is_redist_ospf(self,type,vrouter,command_timeout=5):
		"""Check for if redistributing static routes via OSPF - no ScreenOS devices currently using BGP
                return match object or none"""
 
		self.console.sendline('get vrouter '+vrouter+' protocol ospf rules-redistribute'+self.send_eof)

		try:	
			self.console.expect(self.EOF)
			match = re.search('(.*)\s+'+type,self.console.after.decode())
			return match.group(1)
		except:
			return None
	
	def close(self):
		self.console.sendline('exit')
