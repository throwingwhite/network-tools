#!/usr/bin/env python

from sys import *
from datetime import datetime
import socket, netaddr, re
import routerdb, threading, pathlib
from jnpr.junos import Device
from jnpr.junos import rpcmeta
from jnpr.junos.op.routes import RouteTable
from jnpr.junos.utils.config import Config

user = "user"
passwd = "password"

def main ():
        time = datetime.strftime(datetime.now(), '%Y%m%d_%H%M')

        log = open("cpe"+time+".log",'a')
        error_log = open("cpe"+time+".error.log",'a')

        # To avoid synchronisation issues
        log_lock = threading.RLock()
        rdb_lock = threading.RLock()
        elog_lock = threading.RLock()

        if len(argv) == 2:
                #Parse Rancid routerdb file if it exists
                rdbfile = argv[1]

                if pathlib.Path(rdbfile).is_file():
                        rdb = routerdb.rdb_get(rdbfile)
                else:
                        print("Error: routerdb file does not exist")
        else:
                print("Error: Require path to routerdb")
                exit(1)

        log.write("Loaded "+str(len(rdb))+" routers to process")
        log.flush()

        threads = []

        # Forty threads for the impatient....
        for i in range(40):
                t = threading.Thread(target=process_rdb, args=(rdb,rdb_lock,log_lock,elog_lock,log,error_log))
                threads.append(t)
                t.start()



        log.write("\n")
        log.flush()

def process_rdb(rdb,rdb_lock,log_lock,elog_lock,log,error_log):	

	#rdb_lock.acquire()

	while len(rdb) != 0:
	
		rdb_lock.acquire()
		try:
			router,rtype = rdb.popitem()
		except:
			rdb_lock.release()
			continue
		rdb_lock.release()	

		print("Connecting to: ",router) 


		if rtype == "juniper":

			log_message="\n"+datetime.strftime(datetime.now(), '%H:%M:%S')+" "+router+" "
			login = Device(router,user=user,password=passwd)

			try:
				login.open()
			except ValueError as error:
				elog_lock.acquire()
				error_log.write(log_message+str(error)+"\n")
				error_log.flush()
				elog_lock.release()
				continue
			except:
				elog_lock.acquire()
				raise
				error_log.write(log_message+" Unknown login exception\n")
				error_log.flush()
				elog_lock.release()
				continue
			
			route_table = RouteTable(login)
			route_table.get(protocol='static')
			for route in route_table:
				log_message += str(route.items()) + '\n'

			prefix = login.cli('show config policy-options prefix-list MANAGEMENT-RANGE',warning=False)
			#match = re.findall(r'.*217.196.226.*',prefix)
			print(router+' '+prefix)
			if match:
				print(router,'MANAGEMENT-RANGE Found\n')
				#config = Config(login)
				#set_commands = """
				#set policy-options prefix-list MANAGEMENT-RANGE 185.75.31.128/25 
				#"""
				#config.load(set_commands, format="set")
				#config.commit()
			else:
				print(router,'NOT FOUND\n')		

			#log_message += ' '+str(route_table.keys())

	
			log_lock.acquire()
			log.write(log_message)
			log.flush()
			log_lock.release()
			login.close()


main()
