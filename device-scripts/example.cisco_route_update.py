#!/usr/bin/env python3

from sys import *
from datetime import datetime
import socket, netaddri, pathlib
import device,routerdb,threading


def main ():
        time = datetime.strftime(datetime.now(), '%Y%m%d_%H%M')

        log = open("cpe"+time+".log",'a')
        error_log = open("cpe"+time+".error.log",'a')

        # To avoid synchronisation issues
        log_lock = threading.RLock()
        rdb_lock = threading.RLock()
        elog_lock = threading.RLock()

        if len(argv) == 2:
                #Parse Rancid routerdb file if it exists
                rdbfile = argv[1]

                if pathlib.Path(rdbfile).is_file():
                        rdb = routerdb.rdb_get(rdbfile)
                else:
                        print("Error: routerdb file does not exist")
        else:
                print("Error: Require path to routerdb")
                exit(1)

        log.write("Loaded "+str(len(rdb))+" routers to process")
        log.flush()

        threads = []

        # Forty threads for the impatient....
        for i in range(40):
                t = threading.Thread(target=process_rdb, args=(rdb,rdb_lock,log_lock,elog_lock,log,error_log))
                threads.append(t)
                t.start()



        log.write("\n")
        log.flush()

def process_rdb(rdb,rdb_lock,log_lock,elog_lock,log,error_log):

	while len(rdb) != 0:

                # obtain and process device from RDB	
		rdb_lock.acquire()
		try:
			router,rtype = rdb.popitem()
		except:
			rdb_lock.release()
			continue
		rdb_lock.release()	

		print("Connecting to: ",router) 

		log_message="\n"+datetime.strftime(datetime.now(), '%H:%M:%S')+" "+router+" "

		if rtype == "cisco":
			try:
				login = device.cisco.Connect(router)
			except ValueError as error:
				elog_lock.acquire()
				error_log.write(log_message+str(error)+"\n")
				error_log.flush()
				elog_lock.release()
				continue
			except:
				elog_lock.acquire()
				raise
				error_log.write(log_message+" Unknown login exception\n")
				error_log.flush()
				elog_lock.release()
				continue

                # Obtain MGMT route template
		mgmt_routes = login.get_mgmt_routes('8.8.8.0 255.255.255.240')
		
                # Remove and add new management route
		if mgmt_routes:
			for mgmt_route in mgmt_routes:
				route_command = " ".join(mgmt_route)
				log_message += " MGMT Route: "+ route_command


				if login.config():
					login.delete_mgmt_route(mgmt_route)
					login.set_mgmt_route('9.9.9.9','255.255.255.255',mgmt_route)

					login.config_end()
					if login.config_save():
						log_message+=" Config Saved: True"
					else:
						log_message+=" Config Saved: False"
				else:
					log_message+=" Error entering config mode"
				
			if login.get_mgmt_routes('9.9.9.9 255.255.255.255'):
				log_message+=" Route Updated"
			else:
				log_message+=" Route NOT UPDATED"

		else:
			log_message += " MGMT Route: not found"

		log_lock.acquire()
		log.write(log_message)
		log.flush()
		log_lock.release()
		login.close()
