def rdb_get(repo):
        """ Parse Rancid routerdb file and return a dicitonary with hostname as key and 
        device type as value 

        syntax of routerdb entry: < hostname;device_type;state >  where state is either up or down
        """

	rdb={}	

	rdb_fh = open(repo)
	rdb_list = rdb_fh.read()
	for router in rdb_list.splitlines():
		router = router.strip().split(';')
		if router[0] == '' or router[0][0] == '#' or len(router) != 3:
			continue
		if router[2] == 'up':
                        # only record devices marked as up
			rdb[router[0]] = router[1]

	return rdb
