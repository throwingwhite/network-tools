#!/usr/bin/env python3

from sys import *
from datetime import datetime
import socket, netaddri, pathlib
import device,routerdb,threading


def main ():
	time = datetime.strftime(datetime.now(), '%Y%m%d_%H%M')

	log = open("cpe"+time+".log",'a')
	error_log = open("cpe"+time+".error.log",'a')

	# To avoid synchronisation issues
	log_lock = threading.RLock()
	rdb_lock = threading.RLock()
	elog_lock = threading.RLock()

	if len(argv) == 2:
                #Parse Rancid routerdb file if it exists
                rdbfile = argv[1]

                if pathlib.Path(rdbfile).is_file():
			rdb = routerdb.rdb_get(rdbfile)
		else:
			print("Error: routerdb file does not exist")
	else:
		print("Error: Require path to routerdb")
                exit(1)

	log.write("Loaded "+str(len(rdb))+" routers to process")
	log.flush()	
	
	threads = []

        # Forty threads for the impatient.... 	
	for i in range(40):
		t = threading.Thread(target=process_rdb, args=(rdb,rdb_lock,log_lock,elog_lock,log,error_log))
		threads.append(t)
		t.start()
	


	log.write("\n")
	log.flush()

def process_rdb(rdb,rdb_lock,log_lock,elog_lock,log,error_log):

	while len(rdb) != 0:

                # Obtain device from rdb to process...	
		rdb_lock.acquire()
		try:
			router,rtype = rdb.popitem()
		except:
			rdb_lock.release()
			continue
		rdb_lock.release()	

		print("Processing: ",router) 


		if rtype == "cisco":

			log_message="\n"+datetime.strftime(datetime.now(), '%H:%M:%S')+" "+router+" "

			try:
				login = device.cisco.Connect(router)
			except ValueError as error:
				elog_lock.acquire()
				error_log.write(log_message+str(error)+"\n")
				error_log.flush()
				elog_lock.release()
				continue
			except:
				elog_lock.acquire()
				raise
				error_log.write(log_message+" Unknown login exception\n")
				error_log.flush()
				elog_lock.release()
				continue
                        
                        # Get template management route
			mgmt_routes = login.get_mgmt_routes('8.8.8.(0|64|96)')
		
			if mgmt_routes:
				
                                # Check if redistributing static routes and update redist ACL if exists
				if login.is_redist("static"):
					log_message += " Redist: True"
					acls = login.get_redistribute_acls()
					if acls:
						for acl in acls:
							atype,number = acl[:1],acl[1:]
							if atype == 'a':
								log_message += " ACL: "+number
								seq = login.update_redist_acl(number)
								if seq:
									log_message += " SEQ: "+str(seq)
									login.config()
									login.console.sendline('ip access-list standard '+number)
									login.console.sendline(str(seq)+' deny 9.9.9.0 0.0.0.127')
									login.config_end()
	
							elif atype == 'p':
								log_message += " ACL: prefix list"
							elif atype == 'r':
								log_message += " ACL: route map"
							else:
								log_message += " ACL: could not determine"
					else:
						log_message += " ACL: None" 
				else:
					log_message += " Redist: False"

                                # Add new management routes
				for mgmt_route in mgmt_routes:
					route_command = " ".join(mgmt_route)
					log_message += " MGMT Route: "+ route_command
					login.config()
					login.set_mgmt_route('9.9.9.0','255.255.255.128',mgmt_route)
					login.config_end()
				
				login.config_save()
	
			else:
				log_message += " MGMT Route: not found"

			log_lock.acquire()
			log.write(log_message)
			log.flush()
			log_lock.release()
			login.close()
