#!/usr/bin/env python3

from sys import *
from datetime import datetime
import socket, netaddri, pathlib
import device,routerdb,threading


def main ():
        time = datetime.strftime(datetime.now(), '%Y%m%d_%H%M')

        log = open("cpe"+time+".log",'a')
        error_log = open("cpe"+time+".error.log",'a')

        # To avoid synchronisation issues
        log_lock = threading.RLock()
        rdb_lock = threading.RLock()
        elog_lock = threading.RLock()

        if len(argv) == 2:
                #Parse Rancid routerdb file if it exists
                rdbfile = argv[1]

                if pathlib.Path(rdbfile).is_file():
                        rdb = routerdb.rdb_get(rdbfile)
                else:
                        print("Error: routerdb file does not exist")
        else:
                print("Error: Require path to routerdb")
                exit(1)

        log.write("Loaded "+str(len(rdb))+" routers to process")
        log.flush()

        threads = []

        # Forty threads for the impatient....
        for i in range(40):
                t = threading.Thread(target=process_rdb, args=(rdb,rdb_lock,log_lock,elog_lock,log,error_log))
                threads.append(t)
                t.start()



        log.write("\n")
        log.flush()

def process_rdb(rdb,rdb_lock,log_lock,elog_lock,log,error_log):

	while len(rdb) != 0:

	        # Obtain and process device entry from rdb
		rdb_lock.acquire()
		try:
			router,rtype = rdb.popitem()
		except:
			rdb_lock.release()
			continue
		rdb_lock.release()	

		print("Connecting to: ",router) 


		if rtype == "netscreen":
			
			log_message="\n"+datetime.strftime(datetime.now(), '%H:%M:%S')+" "+router+" "
			
			try:
				login = device.netscreen.Connect(router)
			except ValueError as error:
				elog_lock.acquire()
				error_log.write(log_message+str(error)+"\n")
				error_log.flush()
				elog_lock.release()
				continue
			except:
				elog_lock.acquire()
				raise
				error_log.write(log_message+" Unknown login exception\n")
				error_log.flush()
				elog_lock.release()
				continue

			mgmt_net = '9.9.9.(0|64|96)'

			prefixes =  login.add_mgmt_prefix(mgmt_net)
			if prefixes:
				log_message += str(prefixes)
			else:
				log_message += " NOT FOUND"		

			login.close()
	
			log_lock.acquire()
			log.write(log_message)
			log.flush()
			log_lock.release()
